Minetest 0.4 mod: bucket
=========================

License of source code:
-----------------------
Copyright (C) 2011-2024 Kahrl <kahrl@gmx.net>
Copyright (C) 2011-2024 celeron55, Perttu Ahola <celeron55@gmail.com>
Copyright (C) 2011-2024 Various Minetest developers and contributors

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

http://www.gnu.org/licenses/lgpl-2.1.html

License of media (textures and sounds)
--------------------------------------
Attribution-ShareAlike 4.0 Unported (CC BY-SA 4.0)

http://creativecommons.org/licenses/by-sa/4.0/

Authors of media files
-----------------------
Gambit (CC BY-SA 4.0)  
