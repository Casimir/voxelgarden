local function flower_override(name, def)
	local newgroups = def.groups
	newgroups.dig_immediate = 3
	newgroups.falling_node = 1
	newgroups.snappy = nil
	newgroups.attached_node = nil
	minetest.override_item(name, {
		drawtype = "torchlike",
		groups = newgroups,
	})
end

local function tree_override(name, def)
	local newgroups = def.groups
	newgroups.choppy = 2
	newgroups.snappy = nil
	newgroups.oddly_breakable_by_hand = nil
	minetest.override_item(name, {
		groups = newgroups
	})
end

minetest.register_on_mods_loaded(function()
	for name, def in pairs(minetest.registered_items) do
		if def.groups["flower"] then flower_override(name, def) end
		if def.groups["tree"] then tree_override(name, def) end
	end
end)
