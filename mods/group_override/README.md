# Group override mod

This mod was made specially for Voxelgarden to improve consistency between the
base game content and MTG mods. It overrides items based on their groups, hence
the name.

## License

License of source code: MIT

See LICENSE file for details.
