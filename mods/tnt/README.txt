Minetest Game mod: tnt
======================
See license.txt for license information.

Authors of source code
----------------------
PilzAdam (MIT)
ShadowNinja (MIT)
sofar (sofar@foo-projects.org) (MIT)
Various Minetest developers and contributors (MIT)

Authors of media
---------------------------
BlockMen (CC BY-SA 3.0):
All textures not mentioned below.

ShadowNinja (CC BY-SA 3.0)
  tnt_smoke.png

rudzik8 (CC BY-SA 4.0)
  tnt_tnt_stick.png - Derived from a texture by paramat, which is derived from a texture by benrob0329

Good Morning Craft (CC-BY-SA 4.0)
  All gunpowder textures except tnt_gunpowder_inventory.png
  tnt_blast.png
  tnt_boom.png
  tnt_bottom.png - Color corrected (rudzik8)
  tnt_side.png - Color corrected (rudzik8)
  tnt_top_burning_animated.png - Color corrected (rudzik8)
  tnt_top_burning.png - Color corrected (rudzik8)
  tnt_top.png - Color corrected (rudzik8)

Pixelbox by Gambit (CC BY-SA 3.0)
  tnt_gunpowder_inventory.png


TumeniNodes (CC0 1.0)
  tnt_explode.ogg
  renamed, edited, and converted to .ogg from Explosion2.wav
  by steveygos93 (CC0 1.0)
  <https://freesound.org/s/80401/>

  tnt_ignite.ogg
  renamed, edited, and converted to .ogg from sparkler_fuse_nm.wav
  by theneedle.tv (CC0 1.0)
  <https://freesound.org/s/316682/>

  tnt_gunpowder_burning.ogg
  renamed, edited, and converted to .ogg from road flare ignite burns.wav
  by frankelmedico (CC0 1.0)
  <https://freesound.org/s/348767/>


Introduction
------------
This mod adds TNT to Minetest. TNT is a tool to help the player
in mining.

How to use the mod:

Craft gunpowder by placing coal and gravel in the crafting area.
The gunpowder can be used to craft TNT sticks or as a fuse trail for TNT.

To craft 2 TNT sticks:
G_G
GPG
G_G
G = gunpowder
P = paper
The sticks are not usable as an explosive.

Craft TNT from 9 TNT sticks.

There are different ways to ignite TNT:
  1. Hit it with a torch.
  2. Hit a gunpowder fuse trail that leads to TNT with a torch or
     flint-and-steel.
  3. Activate it with mesecons (fastest way).

For 1 TNT:
Node destruction radius is 3 nodes.
Player and object damage radius is 6 nodes.
